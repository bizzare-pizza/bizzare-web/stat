$(document).ready(function() {
  fetch("http://app.bizzarepizza.xyz/stats/order/list")
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      var p_Config = {
        type: "doughnut",
        data: {
          labels: [],
          datasets: [{
            label: "Кол-во",
            backgroundColor: [],
            data: []
          }]
        },
        options: {
          title: {
            text: "Топ-10 покупаемых блюд"
          },
        }
      };

      function hashCode(str, asString, seed) {
        /*jshint bitwise:false */
        let i, l,
          hval = (seed === undefined) ? 0x811c9dc5 : seed;
        for (i = 0, l = str.length; i < l; i++) {
          hval ^= str.charCodeAt(i);
          hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
        }
        if( asString ){
          // Convert to 8 digit hex string
          return ("0000000" + (hval >>> 0).toString(16)).substr(-8);
        }
        return hval >>> 0;
      }

      function intToRGB(i){
        var c = (i & 0x00FFFFFF)
            .toString(16)
            .toUpperCase();

        return "00000".substring(0, 6 - c.length) + c;
      }

      dataset = {};
      for (let i = 0; i < data.data.length; i++) {
        for (let j = 0; j < data.data[i].dishes.length; j++) {
          if (data.data[i].dishes[j] in dataset) {
            dataset[data.data[i].dishes[j]] += 1;
          } else {
            dataset[data.data[i].dishes[j]] = 1;
          }
        }
      }

      let sortable = [];
      for (let i in dataset) {
          sortable.push([i, dataset[i]]);
      }

      sortable.sort(function(a, b) {
          return b[1] - a[1];
      });

      for (let i = 0; i < sortable.length; i++) {
        if (i < 9) {
          p_Config.data.labels.push(sortable[i][0]);
          p_Config.data.datasets[0]["data"].push(sortable[i][1]);
        } else if (i == 9) {
          p_Config.data.labels.push("Остальное");
          p_Config.data.datasets[0]["data"].push(sortable[i][1]);
        } else {
          p_Config.data.datasets[0]["data"][9] += sortable[i][1];
        }
      }

      for (let i = 0; i < p_Config.data.labels.length; i++) {
        p_Config.data.datasets[0].backgroundColor.push("#" + intToRGB(hashCode(p_Config.data.labels[i], false, 0)));
      }

      let mprice = 0;
      let mwtime = 0;

      for (let i = 0; i < data.data.length; i++) {
        mprice += data.data[i].money;
        mwtime += data.data[i].waiting_time;
      }

      mprice /= data.data.length;
      mwtime /= data.data.length;

      document.getElementById("rInfoCol").setAttribute('style', 'white-space: pre;');
      document.getElementById("rInfoCol").textContent = "Median price: " + Math.floor(mprice) + " \r\nMedian waiting time: " + Math.floor(mwtime);

      var ctx = document.getElementById("pieChart");
      var myChart = new Chart(ctx, p_Config);
    });
});
