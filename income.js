$(document).ready(function() {
  fetch("http://app.bizzarepizza.xyz/stats/order/list")
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      var b_Config = {
        type: "bar",
        data: {
          labels: [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
          ],
          datasets: []
        },
        options: {
          title: {
            text: "Прибыль"
          },
          scales: {
            xAxes: [
              {
                scaleLabel: {
                  labelString: "Месяц"
                }
              }
            ],
            yAxes: [
              {
                scaleLabel: {
                  labelString: "Значение"
                }
              }
            ]
          }
        }
      };
      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, b_Config);

      dataset = {};
      for (let i = 0; i < data.data.length; i++) {
        date = data.data[i].date.split("-");
        if (date[0] in dataset) {
          dataset[date[0]][0].data[parseInt(date[1])-1] += data.data[i].money;
        } else {
          dataset[date[0]] = [{
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            backgroundColor: "#cccccc",
            label: "Гривен"
          }];
          dataset[date[0]][0].data[parseInt(date[1])-1] = data.data[i].money;
        }
      }

      let yearSelect = document.getElementById("yearSelect");
      for (i in dataset) {
        let option = document.createElement("option");
        option.text = i.toString();
        yearSelect.add(option);
      }

      b_Config.data.datasets = dataset[yearSelect.value];
      console.log(b_Config.data.datasets);
      myChart.update();

      yearSelect.onchange = function() {
        let year = this.value;
        myChart.data.datasets = dataset[year];
        myChart.update();
      };
    });
});
