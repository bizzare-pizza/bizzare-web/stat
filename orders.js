// Глобальные объявления

// Основные действия
// document.ready
$(document).ready(function() {
  let t_Config = {
    language: {
      lengthMenu: "Показать _MENU_ записей",
      zeroRecords: "Ничего не найдено",
      info: "Страница _PAGE_ из _PAGES_",
      infoEmpty: "Таблца пуста",
      infoFiltered: "(отфильтровано из _MAX_ записей)"
    },
    columnDefs: [
      { className: "w20", targets: [0] },
      { className: "w40", targets: [1] },
      { className: "w30", targets: [2] },
      { className: "w10", targets: [3] }
    ]
  };

  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce(function(result, key) {
        result[key] = obj[key];
        return result;
      }, {});
  }

  function t_TranslateTime(time) {
    let h = Math.floor(time / 60);
    if (h < 10) {
      h = "0" + h.toString();
    }

    let m = time % 60;
    if (m < 10) {
      m = "0" + m.toString();
    }

    return h + ":" + m;
  }

  function countArrEl(array) {
    o = {};
    for (let i = 0; i < array.length; i++) {
      if (array[i] in o) {
        o[array[i]] += 1;
      } else {
        o[array[i]] = 1;
      }
    }
    return sortObject(o);
  }

  function t_Dishes(dishes) {
    dishes = countArrEl(dishes);
    let s_dishes = "";
    for (let i in dishes) {
      s_dishes += i + " x " + dishes[i].toString() + "<br>";
    }
    return s_dishes;
  }

  console.log("Document ready!");

  fetch("http://app.bizzarepizza.xyz/stats/order/list")
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      // Table //
      let infoTable = $("#infoTable").DataTable(t_Config);

      for (let i = 0; i < data.data.length; i++) {
        infoTable.row
          .add([
            data.data[i].date,
            t_Dishes(data.data[i].dishes),
            data.data[i].address,
            t_TranslateTime(data.data[i].waiting_time)
          ])
          .draw(false);
      }
    });
});
